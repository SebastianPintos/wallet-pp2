package wallet;

public class Wallet {
	private double money;
	private double locked;

	public Wallet(double money) {
		this.money = money;
		this.locked = 0;
	}

	public double addMoney(double money){
		if(money < 0) throw new IllegalArgumentException();
		this.money += money;
		return money;
	}
	
	public double lockMoney(double money){
		if(money < 0 || (this.money - money) < 0 ) throw new IllegalArgumentException();
		this.locked += money;
		return locked;
	}
	
	public void unlockMoney(double money) {
		if((this.locked - money) < 0) throw new IllegalArgumentException();
		this.locked -= money;
	}
	
	public double withdrawMoney(double money){
		if(money < 0 || (this.money - money) < 0) throw new IllegalArgumentException();
		this.money -= money;
		return this.money;
	}
	
	public double getAvailableMoney() {
		return money - locked;
	}
}
