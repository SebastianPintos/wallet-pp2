package wallet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class WalletTest {
	private Wallet wallet;

	@Before
	public void init() {
		wallet = new Wallet(1);
	}
	
    @Test 
    public void addMoneyTestOk() {
        assertEquals(wallet.getAvailableMoney(), 1, 0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void addNegativeMoneyFail() {
    	wallet.addMoney(-1);
    }
    
    @Test 
    public void withdrawMoneyTestOk() {
    	wallet.withdrawMoney(1);
        assertTrue(wallet.getAvailableMoney() == 0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void withdrawMoneyTestWithException() {
    	wallet.withdrawMoney(2);
    }
   
    @Test(expected = IllegalArgumentException.class)
    public void withdrawMoneyNegativeMoneyFail() {
    	wallet.withdrawMoney(-1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void lockNegativeMoneyFail() {
    	wallet.lockMoney(-1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void lockMoneyFail() {
    	wallet.lockMoney(2);
    }
    
    @Test
    public void lockMoneyOk() {
    	wallet.lockMoney(1);
    	assertEquals(wallet.getAvailableMoney(), 0, 0);
    }
    
    @Test
    public void unlockMoneyOk() {
    	wallet.lockMoney(1);
    	wallet.unlockMoney(1);
    	assertEquals(wallet.getAvailableMoney(), 1, 0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void unlockMoneyFail() {
    	wallet.unlockMoney(1);
    }
}
